package elements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dice {
	
	private static Logger logger = LogManager.getLogger(Dice.class.getName());
	
	private DiceValue value;
	
	public Dice() {
		value =  DiceValue.getRandom();
		logger.info("Dice creation");
		logger.debug("Random value created: "+value);
	}
	
	public DiceValue getValue() {
		logger.debug("Dice value returned: "+value);
		return value;
	}

	public void roll() {
		logger.debug("Random value created: "+value);
		value = DiceValue.getRandom();
	}		
	
	public String toString() {
		return value.toString();
	}

	public void setValue(DiceValue value) {
		logger.debug("New Dice value set: "+value);
		this.value=value;
	}
}
