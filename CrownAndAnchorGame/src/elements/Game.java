package elements;

import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class Game {

	private List<Dice> dice;
	private List<DiceValue> values;
	private static Logger logger = LogManager.getLogger(DiceValue.class.getName());
	
	public Game(Dice die1, Dice die2, Dice die3) {
		if (die1 == null || die2 == null || die3 == null){
			logger.error("Could not start game - Null parameter");
			logger.debug("Die1: "+die1+" - Die2: "+die2+" - Die3: "+die3);
			throw new IllegalArgumentException("Dice cannot be null.");
		}
		dice = new ArrayList<Dice>();
		dice.add(die1);
		dice.add(die2);
		dice.add(die3);
		values = new ArrayList<DiceValue>();
		logger.info("Game created");
		logger.debug("Die1: "+die1+" - Die2: "+die2+" - Die3: "+die3);
	}

	public List<DiceValue> getDiceValues() {
		values.clear();
		logger.debug("List cleared");
		for (Dice d : dice) {
			values.add(d.getValue());
			logger.info("Die "+d.getValue()+" added to list");
			logger.debug("");
		}
		return Collections.unmodifiableList(values);
	}	
	
	public int playRound(Player player, DiceValue pick, int bet ) {		
		if (player == null){
			logger.error("Could not play round - Person cannot be null");
			logger.debug("Player cannot be: "+player);
			throw new IllegalArgumentException("Player cannot be null.");
		}
		if (pick == null){
			logger.error("Could not play round - Pick cannot be null");
			logger.debug("Pick cannot be: "+pick);
			throw new IllegalArgumentException("Pick cannot be negative.");
		}
		if (bet <= 0){
			logger.error("Bet must be greater than 0");
			logger.debug("Bet cannot be: "+bet);
			throw new IllegalArgumentException("Bet cannot be negative.");
		}
		
		logger.info("Game round start");
		
		player.takeBet(bet);
		logger.info("Bet taken from player");
		logger.debug("Bet is: "+ bet+ ", balance is now: "+ player.getBalance());
		    
		int matches = 0;
		for ( Dice d : dice) {
			logger.info("Die value before roll: "+ d.getValue());
			d.roll();
			logger.info("die rolled");
			logger.debug("Die value after roll: "+ d.getValue());
			if (d.getValue().equals(pick)) {
				logger.info("Pick matched roll");
				logger.debug("Die value: "+ d.getValue()+", Pick value: "+pick);
				matches += 1;
			}else{
				logger.info("Pick didn't match roll");
				logger.debug("Die value: "+ d.getValue()+", Pick value: "+pick);
			}
		}
		
		int winnings = matches * bet;
		
		logger.debug("Winnings value to be returned: "+ winnings);
		
		if (matches > 0) {			
			player.receiveWinnings(winnings);
			logger.info("Player received winnings");
			player.returnBet(bet);
			logger.info("Player received bet back");
		}
		
		logger.info("Game round end");
        return winnings;		
	}
	
}
