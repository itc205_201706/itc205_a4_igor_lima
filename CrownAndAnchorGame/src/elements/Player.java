package elements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Player {
	private String name;
	private int balance;
	private int limit;
	
	private static Logger logger = LogManager.getLogger(Dice.class.getName());

	
	public Player(String name, int balance) {
		if (name == null || name .isEmpty()){
			logger.error("Name cannot be null");
			logger.debug("Value of name: "+name);
			throw new IllegalArgumentException("Name cannot be null or empty");
		}
		if (balance <= 0){
			logger.error("Balance must be greater than 0");
			logger.debug("Value of balance: "+balance);
			throw new IllegalArgumentException("Balance cannot be negative");
		}
		this.name = name;
		this.balance = balance;
		this.limit = 0;
		
		logger.debug("Value of name: "+name+", value of balance: "+balance+", value of limit: "+ limit);
		logger.info("Player created");
	}
		
	public String getName() { 
		logger.debug("Value of name: "+name);
		return name; 
	}
	
	public int getBalance() { 
		logger.debug("Value of balance: "+balance);
		return balance; 
	}
	
	public int getLimit() { 
		logger.debug("Value of limit: "+limit);
		return limit;
	}
	
	public void setLimit(int limit) {
		if (limit < 0) {
			logger.error("Limit cannot be negative");
			logger.debug("Value of limit: "+limit);
			throw new IllegalArgumentException("Limit cannot be negative.");
		}
		if (limit > balance){
			logger.error("Limit cannot be greater than balance");
			logger.debug("Value of limit: "+limit+", value of balance: "+balance);
			throw new IllegalArgumentException("Limit cannot be greater than balance.");
		}
		this.limit = limit;
		logger.info("Limit updated");
	}

	public boolean balanceExceedsLimit() {
		logger.debug("Value of limit: "+limit+", value of balance: "+balance);
		return (balance > limit);
	}
	
	public boolean balanceExceedsLimitBy(int amount) {
		logger.debug("Value of limit: "+limit+", value of balance: "+balance+", value of amount: "+amount);
		return (balance - amount >= limit);
	}
	
	public void takeBet(int bet) {
		if (bet <= 0){
			logger.error("Bet must be greater than 0");
			logger.debug("Value of bet: "+bet);
			throw new IllegalArgumentException("Bet cannot be negative.");
		}
		if (!balanceExceedsLimitBy(bet)){
			logger.error("Bet is bigger than balance");
			logger.debug("Value of bet: "+bet+", value of balance: "+balance);
			throw new IllegalArgumentException("Placing bet would go below limit.");
		}
		logger.debug("Value of old balance: "+balance+", value of bet: "+bet);
		balance = balance - bet;
		logger.info("Balance updated");
		logger.debug("Value of new balance: "+balance);
	}
	
	public void returnBet(int bet) {
		if (bet <= 0){
			logger.error("Bet must be greater than 0");
			logger.debug("Value of bet: "+bet);
			throw new IllegalArgumentException("Bet cannot be negative.");
		}
		logger.debug("Value of old balance: "+balance+", value of bet: "+bet);
		balance = balance + bet;
		logger.info("Balance updated");
		logger.debug("Value of new balance: "+balance);
	}
	
	public void receiveWinnings(int winnings) {
		if (winnings < 0){
			logger.error("Winnings cannot be negative");
			logger.debug("Value of winnings: "+winnings);
			throw new IllegalArgumentException("Winnings cannot be negative.");
		}
		logger.debug("Value of old balance: "+balance+", value of winnings: "+winnings);
		balance = balance + winnings;
		logger.info("Balance updated");
		logger.debug("Value of new balance: "+balance);
	}
	
	public String toString() {
		return String.format("Player: %s, Balance: %d, Limit: %d", name, balance, limit);
	}

	public void setBalance(int balance) {
		this.balance = balance;		
	}
}
