package elements;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnitTestDiceValue {
	
	DiceValue sut;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetRandom() {
		//setup
		int countCrown = 0;
		int countAnchor = 0;
		int countSpade = 0;
		int countClub = 0;
		int countDiamond = 0;
		int countHeart = 0;
		int other = 0;
		int repeat = 100000;
		
		//exec
		for(int i=0;i<repeat;i++){
			switch(sut.getRandom()){
				case CROWN:
					countCrown++;
					break;
				case ANCHOR:
					countAnchor++;
					break;
				case SPADE:
					countSpade++;
					break;
				case CLUB:
					countClub++;
					break;
				case DIAMOND:
					countDiamond++;
					break;
				case HEART:
					countHeart++;
					break;
				default:
					other++;
					break;
			}
		}
		
		//verifies and asserts
		float percCrown = (float) countCrown/repeat;
		float percAnchor = (float) countAnchor/repeat;
		float percSpade = (float) countSpade/repeat;
		float percClub = (float) countClub/repeat;
		float percDiamond = (float) countDiamond/repeat;
		float percHeart = (float) countHeart/repeat;
		
		assertEquals(0,other);
		assertEquals(0.16,percCrown,0.01);
		assertEquals(0.16,percAnchor,0.01);
		assertEquals(0.16,percSpade,0.01);
		assertEquals(0.16,percClub,0.01);
		assertEquals(0.16,percDiamond,0.01);
		assertEquals(0.16,percHeart,0.01);
	}

}
