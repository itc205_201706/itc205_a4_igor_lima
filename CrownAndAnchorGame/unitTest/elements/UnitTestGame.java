package elements;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnitTestGame {
	
	Game sut;
	Dice d1;
	Dice d2;
	Dice d3;
	List<Dice> dice;
	
	Player player;
	DiceValue playerPick;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		d1 = mock(Dice.class);
		d2 = mock(Dice.class);
		d3 = mock(Dice.class);
		
		player = mock(Player.class);
		playerPick = DiceValue.getRandom();
		
		sut = new Game(d1,d2,d3);
		
	}

	@After
	public void tearDown() throws Exception {
		sut = null;
	}

	@Test
	public void testGame() {
		List<DiceValue> expected = new ArrayList<DiceValue>();
		expected.add(d1.getValue());
		expected.add(d2.getValue());
		expected.add(d3.getValue());
		
		List<DiceValue> actual = sut.getDiceValues();
		
		assertThat(expected,is(actual));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGameNullDie1() {
		sut = new Game(null,d2,d3);
		
		fail("IllegalArgumentException Not Called");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGameNullDie2() {
		sut = new Game(d1,null,d3);
		
		fail("IllegalArgumentException Not Called");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGameNullDie3() {
		sut = new Game(d1,d2,null);
		
		fail("IllegalArgumentException Not Called");
	}

	@Test
	public void testPlayRoundWin3() {
		//setup
		int bet = 5;
		int winnings;
		when(d1.getValue()).thenReturn(DiceValue.ANCHOR);
		when(d2.getValue()).thenReturn(DiceValue.ANCHOR);
		when(d3.getValue()).thenReturn(DiceValue.ANCHOR);
		
		//exec
		winnings = sut.playRound(player, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		verify(player).takeBet(bet);
		verify(d1).roll();
		verify(d2).roll();
		verify(d3).roll();
		verify(player).receiveWinnings(15);
		assertEquals(15,winnings);
	}
	
	@Test
	public void testPlayRoundWin2() {
		//setup
		int bet = 5;
		int winnings;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.ANCHOR);
		when(d3.getValue()).thenReturn(DiceValue.ANCHOR);
		
		//exec
		winnings = sut.playRound(player, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		verify(player).takeBet(bet);
		verify(d1).roll();
		verify(d2).roll();
		verify(d3).roll();
		verify(player).receiveWinnings(10);
		assertEquals(10,winnings);
	}
	
	@Test
	public void testPlayRoundWin1() {
		//setup
		int bet = 5;
		int winnings;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.CROWN);
		when(d3.getValue()).thenReturn(DiceValue.ANCHOR);
		
		//exec
		winnings = sut.playRound(player, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		verify(player).takeBet(bet);
		verify(d1).roll();
		verify(d2).roll();
		verify(d3).roll();
		verify(player).receiveWinnings(5);
		assertEquals(5,winnings);
	}
	
	@Test
	public void testPlayRoundLose() {
		//setup
		int bet = 5;
		int winnings;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.CROWN);
		when(d3.getValue()).thenReturn(DiceValue.CROWN);
		
		//exec
		winnings = sut.playRound(player, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		verify(player).takeBet(bet);
		verify(d1).roll();
		verify(d2).roll();
		verify(d3).roll();
		assertEquals(0,winnings);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPlayRoundBetZero() {
		//setup
		int bet = 0;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.CROWN);
		when(d3.getValue()).thenReturn(DiceValue.CROWN);
		
		//exec
		sut.playRound(player, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		fail("IllegalArgumentException Not Executed");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPlayRoundBetNegative() {
		//setup
		int bet = -5;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.CROWN);
		when(d3.getValue()).thenReturn(DiceValue.CROWN);
		
		//exec
		sut.playRound(player, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		fail("IllegalArgumentException Not Executed");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPlayRoundPickNull() {
		//setup
		int bet = 5;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.CROWN);
		when(d3.getValue()).thenReturn(DiceValue.CROWN);
		
		//exec
		sut.playRound(player, null, bet);
		
		//verifies and asserts
		fail("IllegalArgumentException Not Executed");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPlayRoundPlayerNull() {
		//setup
		int bet = 5;
		when(d1.getValue()).thenReturn(DiceValue.CROWN);
		when(d2.getValue()).thenReturn(DiceValue.CROWN);
		when(d3.getValue()).thenReturn(DiceValue.CROWN);
		
		//exec
		sut.playRound(null, DiceValue.ANCHOR, bet);
		
		//verifies and asserts
		fail("IllegalArgumentException Not Executed");
	}

}
