package elements;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnitTestPlayer {
	
	String name;
	int balance;
	int limit;
	
	Player sut;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		name = "Tester";
		balance = 100;
		
		sut = new Player(name,balance);
	}

	@After
	public void tearDown() throws Exception {
		sut = null;
	}

	@Test
	public void testPlayer() {
		assertEquals(name,sut.getName());
		assertEquals(balance,sut.getBalance());
		assertEquals(0,sut.getLimit());
		
		assertTrue(sut.toString() instanceof String);
	}

	@Test(expected=RuntimeException.class)
	public void testPlayerNullName() {
		
		sut = new Player(null, balance);
		
		fail("Runtime Exception Not Executed");
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPlayerNegativeBalance() {
		
		sut = new Player(name, -10);
		
		fail("IllegalArgumentException Not Executed");
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPlayerBalanceZero() {
		
		sut = new Player(name, 0);
		
		fail("IllegalArgumentException Not Executed");
		
	}

	@Test
	public void testSetLimit() {
		//setup
		int newLimit = 5;
		
		//exec
		sut.setLimit(newLimit);		
		
		//verifies and asserts
		assertEquals(newLimit,sut.getLimit());
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetLimitNegative() {
		//setup
		int negativeLimit = -5;
		
		//exec
		sut.setLimit(negativeLimit);		
		
		//verifies and asserts
		fail("IllegalArgumentException Not Executed");
		
	}
	
	@Test(expected=RuntimeException.class)
	public void testSetLimitGreaterThanBalance() {
		//setup
		int newLimit = 10;
		int newBalance = 5;
		
		//exec
		sut.setBalance(newBalance);
		sut.setLimit(newLimit);
		
		
		//verifies and asserts
		fail("RuntimeException Not Executed");
		
	}
	
	@Test(expected=RuntimeException.class)
	public void testSetLimitEqualToBalance() {
		//setup
		int newLimit = 10;
		int newBalance = 10;
		
		//exec
		sut.setBalance(newBalance);
		sut.setLimit(newLimit);
		
		
		//verifies and asserts
		fail("RuntimeException Not Executed");
		
	}

	@Test
	public void testBalanceExceedsPositiveDifference() {
		//setup
		int newLimit = 1;
		int newBalance = 2;
		int winnings = 10;
		
		//exec
		sut.setBalance(newBalance);
		sut.setLimit(newLimit);
		sut.receiveWinnings(winnings);
		
		//verifies and asserts
		assertTrue(sut.balanceExceedsLimit());
		
	}
	
	@Test
	public void testBalanceExceedsZeroDifference() {
		//setup
		int newLimit = 12;
		int newBalance = 2;
		int winnings = 10;
		
		//exec
		sut.setLimit(newLimit);
		sut.setBalance(newBalance);
		sut.receiveWinnings(winnings);
		
		//verifies and asserts
		assertFalse(sut.balanceExceedsLimit());
		
	}
	
	@Test
	public void testBalanceExceedsLimitNegativeDifference() {
		//setup
		int newLimit = 3;
		int newBalance = 2;
		
		//exec
		sut.setLimit(newLimit);
		sut.setBalance(newBalance);
		
		//verifies and asserts
		assertTrue(sut.getBalance() < sut.getLimit());
		assertFalse(sut.balanceExceedsLimit());
		
	}

	@Test
	public void testBalanceExceedsLimitByPositiveDifference() {
		//setup
		int newLimit = 1;
		int newBalance = 2;
		int winnings = 10;
		int diff = winnings+newBalance-newLimit-1;
		sut.setBalance(newBalance);
		sut.setLimit(newLimit);
		
		//exec
		sut.receiveWinnings(winnings);
		
		//verifies and asserts
		assertTrue(diff<(winnings+newBalance-newLimit));//make sure diff is smaller
		assertTrue(sut.balanceExceedsLimitBy(diff));
	}
	
	@Test
	public void testBalanceExceedsLimitByNegativeDifference() {
		//setup
		int newLimit = 1;
		int newBalance = 2;
		int winnings = 10;
		int diff = winnings+newBalance-newLimit+1;
		sut.setBalance(newBalance);
		sut.setLimit(newLimit);
		
		//exec
		sut.receiveWinnings(winnings);
		
		//verifies and asserts
		assertFalse(diff<(winnings+newBalance-newLimit));//make sure diff is not smaller
		assertFalse(sut.balanceExceedsLimitBy(diff));
	}
	
	@Test
	public void testBalanceExceedsLimitByZeroDifference() {
		//setup
		int newLimit = 1;
		int newBalance = 2;
		int winnings = 10;
		int diff = winnings+newBalance-newLimit;
		sut.setBalance(newBalance);
		sut.setLimit(newLimit);
		
		//exec
		sut.receiveWinnings(winnings);
		
		//verifies and asserts
		assertTrue(diff==(winnings+newBalance-newLimit));//make sure diff is not smaller
		assertTrue(sut.balanceExceedsLimitBy(diff));
	}	

	@Test
	public void testTakeBet() {
		//setup
		int balance = 50;
		int bet = 5;
		
		sut.setBalance(balance);
		
		//exec
		sut.takeBet(bet);
		
		//verifies and asserts
		assertEquals(balance-bet,sut.getBalance()); //make sure diff is not smaller
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTakeBetNegative() {
		//setup
		int balance = 50;
		int bet = -5;
		
		sut.setBalance(balance);
		
		//exec
		sut.takeBet(bet);
		
		//verifies and asserts
		fail("IllegalArgumentException Not Called");
	
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTakeBetZero() {
		//setup
		int balance = 50;
		int bet = 0;
		
		sut.setBalance(balance);
		
		//exec
		sut.takeBet(bet);
		
		//verifies and asserts
		fail("IllegalArgumentException Not Called");
	
	}

	@Test
	public void testReceiveWinnings() {
		//setup
		int balance = 50;
		int winnings = 5;
		
		sut.setBalance(balance);
		
		//exec
		sut.receiveWinnings(winnings);;
		
		//verifies and asserts
		assertEquals(balance+winnings, sut.getBalance());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testReceiveWinningsNegative() {
		//setup
		int balance = 50;
		int winnings = -5;
		
		sut.setBalance(balance);
		
		//exec
		sut.receiveWinnings(winnings);;
		
		//verifies and asserts
		fail("IllegalArgumentException Not Called");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testReceiveWinningsZero() {
		//setup
		int balance = 50;
		int winnings = 0;
		
		sut.setBalance(balance);
		
		//exec
		sut.receiveWinnings(winnings);;
		
		//verifies and asserts
		fail("IllegalArgumentException Not Called");
	}

}
